Data
====

Data can be downloaded from `this link <https://drive.google.com/open?id=1Vua7zd6VuH6jc-NAd1U5iey4wU5bNrm4>`_.

In particular:

* Lossless compressed videos can be downloaded from `this link <https://drive.google.com/open?id=1MSrYBGSOtlwyxMUtUhsLT2HqjW7eQhjH>`_. Raw videos are available upon request from the corresponding author.

* A library of single-individual images of zebrafish to test identification methods can be found in `this link <https://drive.google.com/open?id=1QlV57AmAh1VGgClhDyDAV2Q3t7isD6YF>`_.

* Two example videos, one of 8 adult zebrafish and another of 100 juvenile zebrafish, are also included as part of the :doc:`./quickstart` user guide.

* Minimal processed data to reproduce all figures and tables can be found in `this link <https://drive.google.com/open?id=1PNvgikUgEWG6yioztIoIOH6uvrQIUz-c>`_.

* All tracking results for different species, group sizes, and image quality conditions can be found in `this link <https://drive.google.com/open?id=1VxfoO70dj0Kk6M3ZrAE7Lj7kuYbteiE1>`_.

* Results from tests with the library of single-individual images can be found in `this link <https://drive.google.com/open?id=1LaWRdITJjL2uW-j9jCg126hTyF8kNUxL>`_.
