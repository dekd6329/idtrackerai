Identification network
=============================

^^^^^
idCNN
^^^^^

This module contains the main Tensorflow operations needed to train and
the idCNN

.. automodule:: id_CNN
  :members:

^^^^^^^^^^^^^^^^^^
Network parameters
^^^^^^^^^^^^^^^^^^

.. automodule:: network_params
  :members:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Stop training criteria (idCNN)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: stop_training_criteria
  :members:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Store accurcay and loss
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: store_accuracy_and_loss
  :members:


^^^^^^^^
Get data
^^^^^^^^

.. automodule:: get_data
  :members:

^^^^^^^^^^^^
Epoch runner
^^^^^^^^^^^^

Computes a given operation for an entire epoch divided in batches

.. automodule:: epoch_runner
  :members:


^^^^^^^^^^^^^^^
Get predictions
^^^^^^^^^^^^^^^

Retrieves the predictions of the model idCNN (:class:`.ConvNetwork`) for a set of images

.. automodule:: get_predictions
  :members:
